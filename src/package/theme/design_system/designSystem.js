import { makeStyles } from '@material-ui/core/styles';
import ButtonAtom from './Shared/atoms/Button_atom';
import LabelAtom from './Shared/atoms/Label_atom';
import HeaderMenuMolecule from './Shared/molecules/Header/Menu/HeaderMenu_molecule';
import ProfileButtonMolecule from './Shared/molecules/Header/Menu/ProfileButton_molecule';
import ProfileMenuMolecule from './Shared/molecules/Header/Menu/ProfileMenu_molecule';
import WarningBarMolecule from './Shared/molecules/Header/WarningBar_molecule';
import HeaderOrganism from './Shared/organisms/Header/Header_organism';

const DesignSystem = {
  templates: {},
  atoms: {
    LabelAtom: () => makeStyles(LabelAtom),
    ButtonAtom: () => makeStyles(ButtonAtom),
  },
  molecules: {
    Header: {
      WarningBar: () => makeStyles(WarningBarMolecule),
      ProfileMenuMolecule: {
        ...ProfileMenuMolecule,
      },
      ProfileButtonMolecule: () => makeStyles(ProfileButtonMolecule),
      HeaderMenuMolecule: () => makeStyles(HeaderMenuMolecule),
    },
  },
  organisms: {
    Header: {
      HeaderOrganism: () => makeStyles(HeaderOrganism),
    },
  },
  pages: {},
};

export default DesignSystem;
