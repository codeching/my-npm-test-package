/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
import { fontSizes, fontWeights } from '../../../theme';

const ButtonAtom = (theme) => ({
  button: {
    '&.MuiButton-root': {
      textTransform: 'uppercase',
      boxShadow: 'none',
      borderRadius: 0,
      ...fontWeights.Normal,
      fontSize: fontSizes.Small,
      boxSizing: 'border-box',
      height: 48,
      paddingLeft: 24,
      paddingRight: 24,
      letterSpacing: 1,
      minWidth: 48,
      paddingTop: 0,
      paddingBottom: 0,
    },
    '&.MuiButton-root .MuiButton-label': {
      lineHeight: '48px',
    },
    '&.MuiButton-outlined .MuiButton-label': {
      lineHeight: '46px',
    },
    '&.MuiButton-containedPrimary': {
      backgroundColor: theme.palette.primary.main,
      background: `linear-gradient(90deg, ${theme.palette.primary.light} -5%, ${theme.palette.primary.dark} 100%)`,
    },
    '&.MuiButton-containedPrimary svg path[fill]': {
      fill: theme.palette.common.white,
    },
    '&.MuiButton-containedPrimary svg path[stroke]': {
      stroke: theme.palette.common.white,
    },
    '&.MuiButton-containedPrimary:hover': {
      backgroundColor: theme.palette.primary.dark,
      background: theme.palette.primary.dark,
    },
    '&.MuiButton-outlined': {
      backgroundColor: theme.palette.common.white,
    },
    '&.MuiButton-outlinedPrimary': {
      borderImage: `linear-gradient(90deg, ${theme.palette.primary.light} -5%, #CA0029 100%) 1`,
    },
    '&.MuiButton-outlinedPrimary:hover': {
      borderColor: theme.palette.primary.dark,
      borderImage: 'none',
      backgroundColor: theme.palette.grey['50'],
    },
    '&.MuiButton-outlinedSecondary': {
      borderColor: theme.palette.grey['500'],
      color: theme.palette.grey['800'],
    },
    '&.MuiButton-outlinedSecondary svg path[fill]': {
      fill: theme.palette.grey['800'],
    },
    '&.MuiButton-outlinedSecondary svg path[stroke]': {
      stroke: theme.palette.grey['800'],
    },
    '&.MuiButton-outlinedSecondary:hover': {
      borderColor: theme.palette.grey['400'],
      backgroundColor: theme.palette.grey['50'],
    },
    '&.MuiButton-root.Mui-disabled': {
      background: theme.palette.grey['100'],
      backgroundColor: theme.palette.grey['100'],
      borderColor: theme.palette.grey['300'],
      borderImage: 'none',
    },
    '&.MuiButton-root.Mui-disabled .MuiButton-label': {
      color: theme.palette.grey['500'],
    },
    '&.MuiButton-root.Mui-disabled svg path[fill]': {
      fill: theme.palette.grey['500'],
    },
    '&.MuiButton-root.Mui-disabled svg path[stroke]': {
      stroke: theme.palette.grey['500'],
    },
    '&.MuiButton-text': {
      padding: 0,
      height: 'auto',
    },
    '&.MuiButton-text .MuiButton-label': {
      lineHeight: '16px',
    },
    '&.MuiButton-textSecondary': {
      color: theme.palette.grey[600],
      height: 'auto',
      ...fontWeights.Medium,
    },
    '&.MuiButton-textSecondary svg path[fill]': {
      fill: theme.palette.grey['600'],
    },
    '&.MuiButton-textSecondary svg path[stroke]': {
      stroke: theme.palette.grey['600'],
    },
    '&.MuiButton-textSecondary:hover': {
      backgroundColor: 'transparent',
      color: theme.palette.common.black,
    },
    '&.MuiButton-textSecondary:hover svg path[fill]': {
      fill: theme.palette.common.black,
    },
    '&.MuiButton-textSecondary:hover svg path[stroke]': {
      stroke: theme.palette.common.black,
    },
    '&.MuiButton-textPrimary:hover': {
      backgroundColor: 'transparent',
    },
    '&.MuiButton-textPrimary .MuiButton-label': {
      display: 'inline-block',
      color: theme.palette.primary.main,
      background:
        window && window.matchMedia && !window.matchMedia('(-ms-high-contrast: none)').matches
          ? 'linear-gradient(90deg, #F59601 -5%, #CA0029 100%)'
          : 'transparent',
      '-webkit-background-clip': 'text',
      textFillColor: 'transparent',
      ...fontWeights.Semibold,
    },
  },
  withIcon: {
    '& .MuiButton-label': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  withStartIcon: {},
  withEndIcon: {},
  icon: () => {
    const style = {
      display: 'flex',
      alignItems: 'center',
      position: 'relative',
      top: 0,
      minWidth: 16,
    };

    const userAgentC = navigator.userAgent;
    const isIE =
      userAgentC.indexOf('MSIE ') > -1 || userAgentC.indexOf('Trident/') > -1 || userAgentC.indexOf('Edge/') > -1;

    if (isIE) {
      return {
        ...style,
        left: -2,
      };
    }

    return { ...style, justifyContent: 'center' };
  },
  startIcon: {
    marginRight: 8,
  },
  endIcon: {
    marginLeft: 8,
  },
  iconOnly: {
    '&.MuiButton-root': {
      paddingLeft: 12,
      paddingRight: 12,
    },
    '&.MuiButton-outlinedSecondary': {
      backgroundColor: 'transparent',
    },
    '&.MuiButton-outlinedSecondary svg path[fill]': {
      fill: theme.palette.common.black,
    },
    '&.MuiButton-outlinedSecondary svg path[stroke]': {
      stroke: theme.palette.common.black,
    },
    '&.MuiButton-outlinedSecondary:hover svg path[fill]': {
      fill: theme.palette.grey['800'],
    },
    '&.MuiButton-outlinedSecondary:hover svg path[stroke]': {
      stroke: theme.palette.grey['800'],
    },
  },
  primaryFallback: {
    '&.MuiButton-textPrimary .MuiButton-label': {
      display: 'inline-flex',
      background: 'none',
      ...fontWeights.Medium,
      textFillColor: theme.palette.primary.main,
    },
    '&.MuiButton-text svg path[fill]': {
      fill: theme.palette.primary.main,
    },
    '&.MuiButton-text svg path[stroke]': {
      stroke: theme.palette.primary.main,
    },
  },
});

export default ButtonAtom;
