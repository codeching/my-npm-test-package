const LabelAtom = (theme) => ({
  typography: (props) => {
    const styles = {
      fontSize: props.fontSize,
      fontWeight: props.fontWeight,
    };
    if (props.fontFamily !== null) {
      styles.fontFamily = props.fontFamily;
    }

    if (props.lineHeight !== null) {
      styles.lineHeight = props.lineHeight;
    }

    if (props.type && props.color && theme.palette[props.type] && theme.palette[props.type][props.color]) {
      styles.color = theme.palette[props.type][props.color];
    }

    // Fallback - TODO remove

    if (props.type && props.color && !styles.color && theme[props.type] && theme[props.type][props.color]) {
      styles.color = theme[props.type][props.color];
    }

    return styles;
  },
});

export default LabelAtom;
