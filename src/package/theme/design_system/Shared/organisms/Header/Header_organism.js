import theme, { fontWeights } from '../../../../theme';
import LogoMolecule from '../../molecules/Header/Menu/Logo_molecule';
import NavTabMolecule from '../../molecules/Header/Menu/NavTab_molecule';

const HeaderOrganism = {
  mainNav: {
    overflow: 'hidden',
    padding: '0 80px',
    height: 115,
    background: theme.palette.common.white,
    boxShadow: 'none',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottom: `1px solid ${theme.palette.grey[200]}`,
  },
  navContainer: {
    height: '100%',
    width: '100%',
    '&.MuiToolbar-gutters': {
      padding: 0,
    },
  },
  env: {
    position: 'absolute',
    top: 0,
    left: 0,
    margin: '12px 16px',
    color: theme.palette.grey[600],
    background: theme.palette.grey[200],
    padding: '4px 8px',
    borderRadius: 0,
    fontWeight: 'normal',
  },
  profileMenuItem: {
    color: theme.palette.grey[800],
    lineHeight: '19px',
    ...fontWeights.Medium,
    textTransform: 'uppercase',
  },
  logoutMenuItem: {
    ...fontWeights.Medium,
    textTransform: 'uppercase',
  },
  ...LogoMolecule,
  ...NavTabMolecule,
};

export default HeaderOrganism;
