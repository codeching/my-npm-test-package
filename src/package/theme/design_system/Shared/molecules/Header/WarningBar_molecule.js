const WarningBarMolecule = (theme) => ({
  root: {
    zIndex: 10,
    position: 'relative',

    '& > div:not(.sticky)': {
      transform: 'none !important',
    },
  },
  wrapper: {
    padding: [[0, theme.spacing(1)]],
    height: 40,
    backgroundColor: theme.palette.warning.main,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: theme.palette.common.white,
    overflow: 'hidden',
  },
  truncate: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    display: 'block',
  },
});

export { WarningBarMolecule as default };
