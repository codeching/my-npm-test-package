import TabButtonAtom from '../../../atoms/Header/Menu/TabButton_atom';

const TabButtonMolecule = {
  tabButton: {
    lineHeight: '22px',
    padding: 4,
    marginBottom: 3,
    ...TabButtonAtom,
  },
};

export default TabButtonMolecule;
