import MenuTriangleAtom from '../../../atoms/Header/Menu/MenuTriangle_atom';
import ProfileLogoutTextAtom from '../../../atoms/Header/Menu/ProfileLogoutText_atom';
import ProfileLogoutAtom from '../../../atoms/Header/Menu/ProfileLogout_atom';
import ProfileNameAtom from '../../../atoms/Header/Menu/ProfileName_atom';

const ProfileMenuMolecule = {
  paper: {
    position: 'relative',
    display: 'inline-block',
    maxWidth: 342,
    minWidth: 249,
    height: '133px',
    marginTop: '15px',
    borderRadius: '10px',
    borderTopRightRadius: 0,
    border: '1px solid #C0DDF8',
    overflow: 'visible',
    ...MenuTriangleAtom,
    '& .profileName': {
      padding: '25px 23px 16px 23px',
      ...ProfileNameAtom,
    },
    '& .profileLogout': {
      ...ProfileLogoutAtom,
      height: '48px',
      padding: '0 23px',
    },
    '& .profileLogoutText': {
      ...ProfileLogoutTextAtom,
      lineHeight: '48px',
    },
  },
};

export default ProfileMenuMolecule;
