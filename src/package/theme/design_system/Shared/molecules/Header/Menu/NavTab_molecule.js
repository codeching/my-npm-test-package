import theme, { fontWeights } from '../../../../../theme';

const NavTabMolecule = {
  navTabBar: {
    overflow: 'hidden',
  },
  tabButton: {
    color: `${theme.palette.grey[500]} !important`,
  },
  navTabItemContainer: {
    overflow: 'auto',
    marginLeft: 'auto',
    display: 'flex',
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    '& > *': {
      marginRight: 10,
    },
    '& > a:last-child': {
      marginRight: 0,
    },
    '& a': {
      height: '100%',
      color: 'white',
      textDecoration: 'none',
      display: 'flex',
    },
    '& a > button.MuiButtonBase-root': {
      padding: `0 18px`,
    },
    '& .active > button': {
      marginBottom: 0,
      color: `${theme.palette.common.black} !important`,
      ...fontWeights.Semibold,
      background: `linear-gradient(135.8deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.dark} 98.64%)`,
      backgroundSize: '100% 3px',
      backgroundPosition: 'bottom 0 left 0,bottom 5px left 0',
      backgroundRepeat: 'no-repeat',
    },
  },
  navTabProfileContainer: {
    overflow: 'hidden',
    marginLeft: 14,
    height: '100%',
    position: 'relative',
    '&::before': {
      content: "''",
      display: 'block',
      position: 'absolute',
      left: 0,
      top: '50%',
      marginTop: -(56 / 2),
      height: 56,
      width: 1,
      backgroundColor: theme.palette.grey[400],
    },
  },
  navTabProfileContainerWithoutOrg: {
    marginLeft: 14,
    marginRight: 14,
    height: '100%',
    position: 'relative',
  },
};

export default NavTabMolecule;
