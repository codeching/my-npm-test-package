import theme, { fontWeights } from '../../../../../theme';

const ProfileButtonMolecule = {
  root: {
    maxWidth: 343,
    cursor: 'pointer',
    paddingLeft: 32,
    height: '100%',
    color: theme.palette.common.black,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    paddingLeft: 12,
    boxSizing: 'content-box',
    flexShrink: 0,
  },
  userName: {
    ...fontWeights.Medium,
  },
  textWrapper: {
    overflow: 'hidden',
  },
  truncate: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
};

export default ProfileButtonMolecule;
