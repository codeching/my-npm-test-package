import theme from '../../../../../theme';

const HeaderMenuMolecule = {
  paper: {
    marginTop: 110,
    boxShadow: '0px 4px 14px rgba(0, 0, 0, 0.25)',
    borderRadius: 0,
    background: theme.palette.common.white,
    width: 280,
    right: 80,
    left: 'auto !important',
    '& .MuiMenu-list': {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      '& .MuiListItem-root': {
        padding: '24px 32px',
        margin: 0,
        borderTop: `1px solid ${theme.palette.grey[200]}`,
      },
      '& .MuiListItem-root:first-child': {
        borderTop: 'none',
      },
    },
  },
};

export default HeaderMenuMolecule;
