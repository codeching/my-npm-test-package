import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import React, { forwardRef } from 'react';
import DesignSystem from '../../theme/design_system/designSystem';
import theme from '../../theme/theme';

const useStyles = DesignSystem.atoms.LabelAtom();

const Label = forwardRef((props, ref) => {
  const {
    type,
    color,
    title,
    className,
    variant,
    fontSize,
    fontWeight,
    fontFamily,
    lineHeight,
    onClick,
    children,
    ...other
  } = props;
  let fSize = theme.typography.fontSizes[fontSize] ? theme.typography.fontSizes[fontSize] : null;
  let fWeight = fontWeight;
  if (!fontSize && variant && theme.text[variant]) {
    fSize = theme.text[variant].fontSize;
  }
  if (!fontWeight && variant && theme.text[variant]) {
    fWeight = theme.text[variant].fontWeight;
  }

  if (!fSize) fSize = 'Normal';

  const classes = useStyles({
    type,
    color,
    fontSize: fSize,
    fontWeight: fWeight,
    fontFamily,
    lineHeight,
  });

  return (
    <Typography
      className={`${classes.typography} ${className}`}
      variant={variant}
      fontFamily={fontFamily}
      onClick={onClick}
      {...other}
      ref={ref}
    >
      {children || title}
    </Typography>
  );
});

Label.propTypes = {
  type: PropTypes.string,
  color: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  title: PropTypes.node,
  variant: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.string,
  fontFamily: PropTypes.string,
  lineHeight: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node,
};

Label.defaultProps = {
  type: 'primary',
  color: null,
  title: null,
  variant: 'h2',
  fontSize: null,
  fontWeight: null,
  fontFamily: null,
  lineHeight: null,
  className: '',
  onClick: null,
  children: undefined,
};

export default Label;
