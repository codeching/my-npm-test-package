import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-sticky-el';
import clsx from 'clsx';
import DesignSystem from '../../theme/design_system/designSystem';
import Label from '../labels/Label';

const useStyles = DesignSystem.molecules.Header.WarningBar();

const WraningBar = ({ children, text, className }) => {
  const classes = useStyles();
  return (
    <Sticky className={clsx(className, classes.root)}>
      <div className={classes.wrapper}>
        <Label variant="body1" className={classes.text}>
          <span className={classes.truncate} title={text || String(children)}>
            {text || children}
          </span>
        </Label>
      </div>
    </Sticky>
  );
};

WraningBar.propTypes = {
  children: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.string,
};
WraningBar.defaultProps = {
  children: '',
  text: '',
  className: '',
};

export { WraningBar as default };
