/* eslint-disable no-underscore-dangle */
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { history as historyPropTypes } from '../../common/propTypes';
import PropTypes from 'prop-types';
import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react';
import { Link, useLocation, withRouter } from 'react-router-dom';
import { Menu, MenuItem } from '@material-ui/core';
import DesignSystem from '../../theme/design_system/designSystem';
import clsx from 'clsx';
import CustomButton from '../CustomButton';
import ProfileButton from './ProfileButton';
import WarningBar from './WarningBar';

const useNavConfig = (config, authData) => {
  const location = useLocation();
  const result = React.useMemo(() => {
    const userRoles = authData ? authData.roles : [];
    if (!config) return [];

    return config
      .filter((navConfig) => (navConfig.requiredRole ? userRoles.includes(navConfig.requiredRole) : true))
      .map((item) => {
        const { alias, url } = item;

        const aliasMatches = (() => {
          if (!alias) {
            return false;
          }

          if (Array.isArray(alias)) {
            return alias.some((element) => location.pathname.includes(element));
          }
          return location.pathname.includes(alias);
        })();

        return {
          ...item,
          active: location.pathname.includes(url) || (alias && aliasMatches),
        };
      });
  }, [authData, config, location]);

  return result;
};

const useStyles = DesignSystem.organisms.Header.HeaderOrganism();
const useStylesMenu = DesignSystem.molecules.Header.HeaderMenuMolecule();

const Header = (props) => {
  const {
    config,
    history,
    height,
    showProfileMenu,
    showMenusAndOrg,
    test,
    profileData,
    authData,
    onLogout,
    warningBarText,
    labelMyProfile,
    labelMyOrganization,
    logoutLabel,
    logo,
  } = props;
  const classes = useStyles({ height });

  const headerRef = useRef(null);

  const classesMenu = useStylesMenu();

  const navConfig = useNavConfig(config, authData);
  const [anchorEl, setAnchorEl] = useState(null);
  const [, setProfileOpen] = useState(false);

  const openMenu = useCallback(
    (url) => {
      setAnchorEl(false);
      history.push(url);
    },
    [history]
  );

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setProfileOpen(true);

    setTimeout(() => {
      document.body.style.paddingRight = 0;
      document.body.style.overflow = 'inherit';
    });
  };

  const handleClose = () => {
    setAnchorEl(null);
    setProfileOpen(false);
  };

  // We hide the menu on scroll
  useEffect(() => {
    const handleScroll = () => {
      setAnchorEl(null);
    };
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const navigateToHome = useCallback((e) => {
    e.preventDefault();
    window.location.replace('/');
  }, []);

  const profileName = useMemo(() => {
    if (!profileData && !authData) return '';

    if (localStorage.getItem('organizationId') && (profileData.nickname || profileData.name)) {
      return profileData.nickname || profileData.name;
    }
    return authData.tokenEmail;
  }, [profileData, authData]);

  return (
    <React.Fragment>
      {warningBarText && <WarningBar>{warningBarText}</WarningBar>}
      <AppBar ref={headerRef} position="relative" data-testid="header" elevation={0}>
        <div className={classes.mainNav}>
          <div className={classes.logoContainer}>
            <Link onClick={navigateToHome} to="/">
              {logo}
            </Link>
          </div>
          <Toolbar className={clsx(classes.navTabBar, classes.navContainer)}>
            <div className={classes.navTabItemContainer}>
              {showMenusAndOrg &&
                (navConfig || []).map((configItem, index) => {
                  const { name, url, active } = configItem;

                  return (
                    <Link key={name} to={url} className={(test && index === 0) || active ? 'active' : ''}>
                      <CustomButton variant="text" color="secondary" disableRipple className={classes.tabButton}>
                        {name}
                      </CustomButton>
                    </Link>
                  );
                })}
            </div>
            {showMenusAndOrg && showProfileMenu && (
              <div className={classes.navTabProfileContainer} data-testid="profile-dropdown">
                <ProfileButton
                  aria-controls="customized-menu"
                  id="profile-dropdown"
                  data-testid="innerthing"
                  aria-haspopup="true"
                  variant="contained"
                  color="primary"
                  onClick={handleClick}
                  userName={profileData ? profileData.nickname || profileData.name : ''}
                  organization={profileData ? profileData.organization.name : ''}
                />

                <Menu classes={classesMenu} anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
                  <MenuItem
                    onClick={() => openMenu('/user/profile')}
                    className={classes.profileMenuItem}
                    data-testid="contextmenu-logout"
                  >
                    {labelMyProfile}
                  </MenuItem>
                  <MenuItem
                    onClick={() => openMenu('/user/organization')}
                    className={classes.profileMenuItem}
                    data-testid="contextmenu-logout"
                  >
                    {labelMyOrganization}
                  </MenuItem>

                  {onLogout && (
                    <MenuItem onClick={onLogout} className={classes.logoutMenuItem} data-testid="contextmenu-logout">
                      {logoutLabel}
                    </MenuItem>
                  )}
                </Menu>
              </div>
            )}
            {!showMenusAndOrg && showProfileMenu && onLogout && (
              <div className={classes.navTabProfileContainerWithoutOrg} data-testid="profile-dropdown">
                <ProfileButton
                  aria-controls="customized-menu"
                  id="profile-dropdown"
                  data-testid="innerthing"
                  aria-haspopup="true"
                  variant="contained"
                  color="primary"
                  onClick={handleClick}
                  userName={profileName}
                />

                <Menu classes={classesMenu} anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
                  <MenuItem onClick={onLogout} className={classes.logoutMenuItem} data-testid="contextmenu-logout">
                    {logoutLabel}
                  </MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </div>
      </AppBar>
    </React.Fragment>
  );
};

Header.propTypes = {
  logo: PropTypes.node.isRequired,
  height: PropTypes.number,
  showProfileMenu: PropTypes.bool,
  showMenusAndOrg: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.object,
    isExact: PropTypes.bool,
    path: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  test: PropTypes.bool,
  history: historyPropTypes.isRequired,
  onLogout: PropTypes.func,
  config: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      icon: PropTypes.string,
      active: PropTypes.bool,
      name: PropTypes.string,
      requiredRole: PropTypes.string,
    })
  ),
  profileData: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    status: PropTypes.string,
    email: PropTypes.string,
    nickname: PropTypes.string,
    phone: PropTypes.string,
    organization: PropTypes.shape({
      id: PropTypes.string,
      type: PropTypes.string,
      name: PropTypes.string,
      prefix: PropTypes.string,
      email: PropTypes.string,
      phone: PropTypes.string,
      website: PropTypes.string,
      status: PropTypes.string,
    }),
  }),
  warningBarText: PropTypes.string,
  authData: PropTypes.shape({
    roles: PropTypes.array,
  }),
  labelMyProfile: PropTypes.string,
  labelMyOrganization: PropTypes.string,
  logoutLabel: PropTypes.string,
};

Header.defaultProps = {
  showProfileMenu: true,
  showMenusAndOrg: true,
  height: 414,
  test: false,
  warningBarText: null,
  config: undefined,
  profileData: undefined,
  authData: undefined,
  labelMyProfile: undefined,
  labelMyOrganization: undefined,
  logoutLabel: undefined,
  onLogout: undefined,
};

export default withRouter(Header);
