import React from 'react';
import PropTypes from 'prop-types';
import Label from '../labels/Label';
import DesignSystem from '../../theme/design_system/designSystem';
import clsx from 'clsx';
import { DownArrowProfile } from '../../assets/svg';

const useStyles = DesignSystem.molecules.Header.ProfileButtonMolecule();

const ProfileButton = ({ userName, organization, onClick }) => {
  const classes = useStyles();
  return (
    <div data-testid="profile-open" role="presentation" className={classes.root} onClick={onClick}>
      <div className={classes.textWrapper}>
        <Label className={clsx(classes.userName, classes.truncate)} title={userName} variant="body2" />
        {organization && (
          <Label title={organization} className={classes.truncate} variant="body2" type="grey" color="500" />
        )}
      </div>
      <DownArrowProfile className={classes.icon} />
    </div>
  );
};

ProfileButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  userName: PropTypes.string.isRequired,
  organization: PropTypes.string,
};

ProfileButton.defaultProps = {
  organization: null,
};

export default ProfileButton;
