import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import DesignSystem from '../../theme/design_system/designSystem';

const ProfileMenu = withStyles(DesignSystem.molecules.Header.ProfileMenuMolecule)((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
));

export default ProfileMenu;
