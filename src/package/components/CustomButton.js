import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import React from 'react';
import DesignSystem from '../theme/design_system/designSystem';
import clsx from 'clsx';

const useStyles = DesignSystem.atoms.ButtonAtom();

const CustomButton = ({
  type,
  variant,
  color,
  children,
  disableRipple,
  inputProps,
  className,
  startIcon,
  endIcon,
  iconOnly,
  dataTestId,
  onClick = (f) => f,
  ...rest
}) => {
  const classes = useStyles({ variant, color });

  let buttonClass = '';

  if (startIcon) {
    buttonClass = clsx(buttonClass, classes.withIcon, classes.withStartIcon);
  } else if (endIcon) {
    buttonClass = clsx(buttonClass, classes.withIcon, classes.withEndIcon);
  }

  if (iconOnly) {
    buttonClass = clsx(buttonClass, classes.iconOnly);
  }

  let buttonColor = color;

  if (color === 'primaryFallback') {
    buttonColor = 'primary';
    buttonClass = clsx(buttonClass, classes.primaryFallback);
  }

  return (
    <Button
      className={clsx(className, classes.button, buttonClass)}
      variant={variant}
      disableRipple={disableRipple}
      color={buttonColor}
      onClick={onClick}
      {...inputProps}
      {...rest}
      data-testid={dataTestId}
      type={type}
    >
      {startIcon && <span className={clsx(classes.icon, classes.startIcon)}>{startIcon}</span>}
      {children}
      {endIcon && <span className={clsx(classes.icon, classes.endIcon)}>{endIcon}</span>}
    </Button>
  );
};

CustomButton.propTypes = {
  type: PropTypes.string,
  variant: PropTypes.oneOf(['contained', 'outlined', 'text']),
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary', 'primaryFallback']),
  onClick: PropTypes.func,
  disableRipple: PropTypes.bool,
  children: PropTypes.node,
  /* eslint-disable react/forbid-prop-types */
  inputProps: PropTypes.object,
  className: PropTypes.string,
  startIcon: PropTypes.node,
  endIcon: PropTypes.node,
  iconOnly: PropTypes.bool,
  dataTestId: PropTypes.string,
};

CustomButton.defaultProps = {
  children: undefined,
  type: 'button',
  variant: 'contained',
  color: 'primary',
  disableRipple: true,
  onClick: () => {},
  inputProps: {},
  className: '',
  startIcon: null,
  endIcon: null,
  iconOnly: false,
  dataTestId: undefined,
};

export default CustomButton;
