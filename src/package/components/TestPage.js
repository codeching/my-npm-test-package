import React, { useMemo } from 'react';
import { Logo } from '../assets/svg/index';
import CustomButton from '../components/CustomButton';
import Header from '../components/header/Header';
import { makeStyles } from '@material-ui/styles';

const useStyle = makeStyles({
  main: {
    margin: 20,
  },
});

const TestPage = () => {
  const classes = useStyle();

  const headerConfig = useMemo(
    () => [
      {
        url: '/test',
        icon: null,
        active: false,
        name: 'Test page',
        requiredRole: null,
      },
      {
        url: '/menu1',
        icon: null,
        active: false,
        name: 'Menu 1',
        requiredRole: null,
      },
      {
        url: '/menu2',
        icon: null,
        active: false,
        name: 'Menu 2',
        requiredRole: null,
      },
    ],
    []
  );

  const headerProfileData = useMemo(() => {
    return {
      id: 'codeching',
      name: 'Superadmin',
      status: 'active',
      email: 'codeching@test.com',
      nickname: null,
      phone: '99999999',
      roleId: null,
      organization: {
        id: 'codeching',
        type: 'internal',
        name: 'codeching',
        prefix: null,
        email: 'info@lebersoftware.com',
        phone: '065432213',
        website: null,
        status: 'active',
      },
      pendingEmail: null,
    };
  }, []);

  const headerAuthData = useMemo(() => {
    return {
      roles: [],
      tokenEmail: 'info@lebersoftware.com',
    };
  }, []);

  return (
    <React.Fragment>
      <Header
        test
        logo={<Logo />}
        config={headerConfig}
        profileData={headerProfileData}
        authData={headerAuthData}
        t={(key) => key}
        labelMyProfile="Profile"
        labelMyOrganization="Organization"
        onLogout={() => {
          alert('Log out');
        }}
        logoutLabel="Logout"
      />
      <div className={classes.main}>
        <CustomButton variant="contained" color="primary">
          Call to action
        </CustomButton>
      </div>
    </React.Fragment>
  );
};

export default TestPage;
